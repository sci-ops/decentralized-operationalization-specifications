
## Notes

Items are DOS-leaves

A DOS can contain zero or more items.

A DOS Container (DOSC) can contain zero or more DOSes. <- decide whether to do it like this or just make DOSes recursive

A param_spec defines parameters.

A DOS has a shared stimulus stack that items can draw from and that can be used in the DOS procedure specification.

Regarding `varname`, should there be a 'canonical' version and a 'shorthand' version?

Should we let items inherit from each other? With implicit inheritance from the SSS specified in the DOS, but the ability to add more sources from which the item inherits?
