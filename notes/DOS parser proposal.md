# Terminology
|term|definition|
|-|-|

# intro: The problem
One of the problems encountered in social sciences reseach is reproducability of instrument validation. The lack of reproducability problem may stem from interpretation incongruities between conceptual measurement instruments. In many publications instruments are described in an incomplete and often vague way. Particularly the constructs that instruments should measure can be interpreted in multiple ways.

An example of this is the measurement of the construct _cognitive load_. This construct was first described by XXX and has been referred to in several publications (a, b, c). Instruments for measuring _cognitive load_ have also been proposed and described. However, no concrete description of such instruments 

# A solution
A potential solution to the reproducability/validation problem is a formal language for describing measurement instruments. This formal language should be unambiguous and should be coupled with solutions that provide a deterministic implementation of the described instrument. This means that, whatever the process used to produce the final instrument in it's intended medium, the results should always be the same without any unintended variations that might alter the instrument's measurement results. The word 'medium' refers to the carrier that presents or holds the final instrument. Such as for example: paper, a screen, audio producing devices, a physical device such as a response button, etc.

# criteria:
The solution shoul provide the following features:

To reach the intended situation four elements are needed: (1) a structured and fixed formal language in which the current range of social science measurement instruments can be expressed and that is versioned to allow for further development and referencing, (2) a reliable and modular approach to extending the formal language in such a way that it can incorporate new instruments, new instrument components and new mediums, (3) a deterministic parser to translate formally described instruments into produced instruments, and (4) a way formal way to refer to described instruments and their adapters that allows for the modifications of the original description that are unambiguous and wherein the effects of the modifications can be investigated.
# An example implementation: DOS to A4-pdf
## DOS
DOS are a YAML-based descriptions of social science measurement instruments. DOS consists of a medium description or reference to a medium format document (i.e. an external medium description) and a descripton of the measurement instrument itself.

### Instrument components
**Free-input  items**
Free-input items are input fields of a pre-determined width and height with a surrounding border in which handwritten text may be entered.

**Likert-question items**
Likert-question items are question texts followed by a number of tick-boxes.

## Sources
- [How to write a programming language](https://lisperator.net/pltut/)

## The goal
To take a description of an instrument and to output it as a fully formatted A4-sized pdf that can be printed.

## An example DOS
[A dos file](../dos-specs/direct_attitude_752g9ztj.dos.yml)

## Prerequisites
All required packages should be installed though pip (the most common pyton package manager). This example implementation relies on the following packages:

|package|functionality|
|-|-|
|pyyaml|parse yaml|
|pandas||

## Parser
Python has an available YAML parser that can translate YAML files into python dict objects: the python yaml package

```python
# This is a DOS adapter  
  
import os  
import sys  
import yaml  
  
  
def parse_yaml(yaml_file):  
    dos = []  
    with open(yaml_file, "r") as stream:  
        yaml_stream = yaml.safe_load(stream)  
    return yaml_stream  
  
  
if __name__ == '__main__':  
    path = sys.argv[1]  
    print(path)  
    temp = parse_yaml(path)  
    print(type(temp))  
    print(temp)
```